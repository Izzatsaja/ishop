<?= $this->extend('templates/front_layout') ?>

<?= $this->section('main-content') ?>

<div class="col-12">
            <a href="/cart" class="btn btn-sm btn-info float-right">Back</a>
              <h2>Checkout</h2>
              <hr>
<div class="row">
        <div class="col-12">
        
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th> </th>
                        <th>Product</th>
                        <th>Price</th>
                        <th width="15%">Qty</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>

<?php if(isset($_SESSION['cart']['items']) && (  count($_SESSION['cart']['items']) > 0 ) ) : ?>                
    <?php $counter = 0; ?>
    <?php $total_amount = 0; ?>
    <?php foreach( $_SESSION['cart']['items'] as $item) : ?>
                <tr>
                        <td><?= ++$counter;?></td>
                        <td><?= $item['Nama'] ?></td>
                        <td><?=  number_format( $item['Price'], 2) ?></td>
                        <td><?= $item['qty']?></td>
                        <td>RM <?= number_format($item['Price'] * $item['qty'], 2)?></td>
                    </tr>
    <?php $total_amount += ( $item['Price'] * $item['qty'] ); ?>
    <?php endforeach; ?>
    <tr>
        <td align="right" colspan="4"><strong>Total Amount</strong></td>
        <td><strong>RM <?= number_format( $total_amount ,2); ?></strong></td>
    </tr>
<?php else : ?>

    <tr>
        <td colspan="5" class = "text-center">
            Your cart is empty
        </td>
    </tr>

<?php endif; ?>
                </tbody>
            </table>



        </div>
    </div>

    <hr>

    <?= form_open('/checkout'); ?>

<h3>Maklumat Pelanggan</h3>

<?php if (isset($_SESSION['error'])) :?>
            <div class="row">
                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Masalah dengan maklumat!</strong> Sila semak semula maklumat yang diberikan.</a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
<?php endif; ?>

<?php
    $nama_penuh_valid = '';
    $nama_penuh_value = '';
    if (isset($_SESSION['error'])) {
        $nama_penuh_value = $_SESSION['POST']['nama_penuh'];
        if (isset($_SESSION['form_error']['nama_penuh'])) {
            $nama_penuh_valid = ' is-invalid';
        } else {
            $nama_penuh_valid = ' is-valid';
        }
    }
?>
  <div class="form-group">
    <label for="nama_penuh">Nama Penuh</label>
    <input type="text" class="form-control <?= $nama_penuh_valid?>" id="nama_penuh" name="nama_penuh" value="<?= $nama_penuh_value?>">
  </div>

    <div class="row">
    <?php
    $nombor_telefon_valid = '';
    $nombor_telefon_value = '';
    if (isset($_SESSION['error'])) {
        $nombor_telefon_value = $_SESSION['POST']['nombor_telefon'];
        if (isset($_SESSION['form_error']['nombor_telefon'])) {
            $nombor_telefon_valid = ' is-invalid';
        } else {
            $nombor_telefon_valid = ' is-valid';
        }
    }
?>
        <div class="form-group col-sm-6">
            <label for="nombor_telefon">Nombor Telefon</label>
            <input type="text" class="form-control <?= $nombor_telefon_valid?>" id="nombor_telefon" name="nombor_telefon" value="<?= $nombor_telefon_value?>">
        </div>

        <?php
    $email_valid = '';
    $email_value = '';
    if (isset($_SESSION['error'])) {
        $email_value = $_SESSION['POST']['email'];
        if (isset($_SESSION['form_error']['email'])) {
            $email_valid = ' is-invalid';
        } else {
            $email_valid = ' is-valid';
        }
    }
?>

        <div class="form-group col-sm-6">
            <label for="email">Email</label>
            <input type="email" class="form-control <?= $email_valid?>" id="email" name="email" value="<?= $email_value; ?>">
        </div>
    </div>

    <?php
    $alamat_1_valid = '';
    $alamat_1_value = '';
    if (isset($_SESSION['error'])) {
        $alamat_1_value = $_SESSION['POST']['alamat_1'];
        if (isset($_SESSION['form_error']['alamat_1'])) {
            $alamat_1_valid = ' is-invalid';
        } else {
            $alamat_1_valid = ' is-valid';
        }
    }
?>


  <div class="form-group">
    <label for="alamat_1">Alamat</label>
    <input type="text" class="form-control <?= $alamat_1_valid?>" id="alamat_1" name="alamat_1" value="<?= $alamat_1_value?>">
    <input type="text" class="form-control mt-2" id="alamat_2" name="alamat_2">
  </div>

  <div class="row">

  <?php
    $poskod_valid = '';
    $poskod_value = '';
    if (isset($_SESSION['error'])) {
        $poskod_value = $_SESSION['POST']['poskod'];
        if (isset($_SESSION['form_error']['poskod'])) {
            $poskod_valid = ' is-invalid';
        } else {
            $poskod_valid = ' is-valid';
        }
    }
?>

        <div class="form-group col-sm-2">
            <label for="poskod">Poskod</label>
            <input type="text" class="form-control <?= $poskod_valid?>" id="poskod" name="poskod" value="<?= $poskod_value?>">
        </div>


        <?php
    $daerah_valid = '';
    $daerah_value = '';
    if (isset($_SESSION['error'])) {
        $daerah_value = $_SESSION['POST']['daerah'];
        if (isset($_SESSION['form_error']['daerah'])) {
            $daerah_valid = ' is-invalid';
        } else {
            $daerah_valid = ' is-valid';
        }
    }
?>
        <div class="form-group col-sm-5">
            <label for="daerah">Daerah</label>
            <input type="text" class="form-control <?= $daerah_valid?>" id="daerah" name="daerah" value="<?= $daerah_value?>">
        </div>

        <?php
    $negeri_valid = '';
    $negeri_value = null;
    if (isset($_SESSION['error'])) {
        $negeri_value = $_SESSION['POST']['negeri'];
        if (isset($_SESSION['form_error']['negeri'])) {
            $negeri_valid = ' is-invalid';
        } else {
            $negeri_valid = ' is-valid';
        }
    }
?>

        <div class="form-group col-sm-5">
            <label for="negeri">Negeri</label>
            <?= form_dropdown('negeri', $negeri, $negeri_value, [ 'class' => "form-control $negeri_valid"]);?>
        </div>
    </div>


  <button type="submit" class="btn btn-primary">Bayar dengan Online Banking</button>
</form>


        </div>
    </div>
</div>


   
<?= $this->endsection() ?>