<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="/css/admin.css" >

    <title>Senarai Pikcer</title>
</head>
<body>
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">
            <img src="img/gallogosar.png" alt="" width="40" height="40" class="d-inline-block align-text-top">
            Galleria
          </a>
        </div>
      </nav>
      <div class="container mt-5">
      <?php if (isset($_SESSION['success'])) :?>
         <div class="row">
                <div class="col">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> New data has been added.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
                </div>
            </div>
         </div>

       <?php endif; ?>
       <?php if (isset($_SESSION['deleted'])) :?>
            <div class="row">
                <div class="col">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Success!</strong> Data has been deleted.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                </div>
            </div>

            <?php endif; ?>


        <div class="row">
            <div class="col-12">
              <a href="/Image/add" class="btn btn-sm btn-primary float-right">Add New</a>
              <h3>Senarai Gambar</h3>
            </div>

            <div class="col-12">

              <table class="table table table-hover">
                  <thead class="thead-dark">
                      <tr>
                          <th>ID</th>
                          <th>Gambar</th>
                          <th>Nama</th>
                          <th> </th>
                      </tr>
                  </thead>
                  <tbody>
<?php foreach($image as $i) : ?>                    
                        <tr>
                            <td><?= $i['id'];?></td>
                            <td>
                            <img class="pikcer-daerah" src="/img/<?= $i['Image_loc']?>" alt="">
                            </td>
                            <td>
                            <?= $i['Nama']?>
                            </td>
                            <td>
                                <a href="/Image/edit/<?= $i['id']?>" class="btn btn-sm btn-primary">Edit</a>
                                <button href="/Image/delete/<?= $i['id'];?>" onclick="confirm_delete( <?= $i['id'];?> )"  class="btn btn-sm btn-danger">Delete</button>

                            </td>
                        </tr>
<?php endforeach; ?>
                    </tbody>
                </table>
                
                <div id="my-pagination">
                <?= $pager->links() ?>
                </div>
                 

            </div>
        </div>


    </div>
  

    <footer class="text-center p-5">
      <p>Bohjak copyright &copy; 2021</p>
      
      </footer>
      
    <script>
    function confirm_delete( id ) {
    if ( confirm( 'Are you sure you want to delete record ID '+ id + '?' ) ) {
        window.location.href = '/Image/delete/' + id;
    }
    }
    </script>


</body>
</html>