<?= $this->extend('templates/admin_layout') ?>

<?= $this->section('main-content') ?>
      

      
      <div class="container mt-5">
          <div class="row">

            <div class="col-12">
            <a href="/product" class="btn btn-sm btn-info float-right">Back</a>
              <h3>Tambah Product</h3>
              <hr>

              <?php echo form_open_multipart('/product/add') ?>
                <div class="form-group row">
                  <label for="Nama" class="col-sm-2 col-form-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="Nama" name="Nama" value="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="nama" class="col-sm-2 col-form-label">Price</label>
                  <div class="col-sm-10">
                    <!-- <input type="text" class="form-control" id="price" name="price" value=""> -->

                    <input type="number" class="form-control" required name="Price" id="Price" min="0" step=".01">  

                  </div>
                </div>

                <div class="form-group row">
                  <label for="nama" class="col-sm-2 col-form-label">Category</label>
                  <div class="col-sm-10">

                  <?php 
                    echo form_dropdown('category_id', 
                      $category, null, 
                      [ 'class' => 'form-control' ]
                    );
                  ?>

                  </div>
                </div>

                <div class="form-group row">
                    <label for="Description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">

                        <textarea class="form-control" id="Description" name="Description" rows="3"></textarea>


                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Picture</label>
                    <div class="col-sm-10">

                   <?php
                    $picture_url = '/img/product/'.$product['Picture'];
                    if (!file_exists( 'img/product/'. $product['Picture'])) {
                    $picture_url = '/img/product/default.jpg';
                    }
                  ?> 

                <img src="<?= $picture_url;?>" alt="" style="max-width: 300px;" class="img-fluid">
                  <div class="input-group mb-3 mt-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                     </div>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="Picture" name="Picture">
                          <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                      </div>
                  </div>
                          <button class="btn btn-primary" type="submit">Save</button>

                    </div>
                  </div>


              </form>
            </div>
            </div>
      </div>
    

<?= $this->endSection(); ?>