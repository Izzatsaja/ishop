<?= $this->extend('templates/admin_layout') ?>

<?= $this->section('main-content') ?>

      <div class="container mt-5">
      <?php if (isset($_SESSION['success'])) :?>
         <div class="row">
                <div class="col">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> New data has been added.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
                </div>
            </div>
         </div>

       <?php endif; ?>
       <?php if (isset($_SESSION['deleted'])) :?>
            <div class="row">
                <div class="col">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Success!</strong> Data has been deleted.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                </div>
            </div>

            <?php endif; ?>


        <div class="row">
            <div class="col-12">
              <a href="/Product/add" class="btn btn-sm btn-primary float-right">Add New</a>
              <h3>Product List</h3>
            </div>

            <div class="col-12">

              <table class="table table table-hover">
                  <thead class="thead-dark">
                      <tr>
                          <th>ID</th>
                          <th>Gambar Product</th>
                          <th>Nama</th>
                          <th>Category</th>
                          <th>Price </th>
                          <th> </th>
                      </tr>
                  </thead>
                  <tbody>
<?php $counter = 0; ?>
<?php foreach($product as $i) : ?>                    
                        <tr>
                            <td><?= ++$counter;?></td>
                            <td>
                            <img class="pikcer-daerah" src="/img/product/<?= $i['Picture']?>" alt="">
                            </td>
                            <td><?= $i['Nama']?></td>
                            <td><?= $category[ $i['category_id'] ]?></td>
                            <td>RM <?= number_format($i['Price'],2)?></td>
                            <td>
                                <a href="/Product/edit/<?= $i['id']?>" class="btn btn-sm btn-primary">Edit</a>
                                <button href="/Product/delete/<?= $i['id'];?>" onclick="confirm_delete( <?= $i['id'];?> )"  class="btn btn-sm btn-danger">Delete</button>
                            </td>
                        </tr>
<?php endforeach; ?>
                    </tbody>
                </table>
                
                <div id="my-pagination">
                <?= $pager->links() ?>
                </div>
                 

            </div>
        </div>


    </div>
  

    <footer class="text-center p-5">
      <p>Bohjak copyright &copy; 2021</p>
      
      </footer>
      
    <script>
    function confirm_delete( id ) {
    if ( confirm( 'Are you sure you want to delete record ID '+ id + '?' ) ) {
        window.location.href = '/product/delete/' + id;
    }
    }
    </script>


<?= $this->endSection() ?>