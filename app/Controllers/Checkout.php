<?php

namespace App\Controllers;

class Checkout extends BaseController
{
    var $negeri = [
        'WP Kuala Lumpur',
        'WP Putrajaya',
        'WP Labuan',
        'Selangor',
        'Negeri Sembilan',
        'Melaka',
        'Johor',
        'Pahang',
        'Terengganu',
        'Kelantan',
        'Perlis',
        'Pulau Pinang',
        'Kedah',
        'Perak',
        'Sabah',
        'Sarawak'        
    ];

    function __construct() {
        $this->validation =  \Config\Services::validation();
        $negeri2 = [];

        foreach($this->negeri as $k => $v) {
            $negeri2[ $v ] = $v;
        }

        $this->data = [
            'negeri' => $negeri2
        ];
		$this->session = session();
	}


    // /bakul --- display items in cart
    public function index() {
        $this->validate([]);
        $this->data['validation'] = $this->validator;
		return view('checkout', $this->data);
	}

    private function get_cart_total_amount() {
        $total_amount = 0;
        if (isset($_SESSION['cart']['items'])) {
            foreach($_SESSION['cart']['items'] as $item) {
                $total_amount += $item['Price'] * $item['qty'];
            }
        }
        return $total_amount;
    }

    private function get_cart_qty() {
        $total_qty = 0;
        if (isset($_SESSION['cart']['items'])) {
            foreach($_SESSION['cart']['items'] as $item) {
                $total_qty += $item['qty'];
            }
        }
        return $total_qty;
    }

    private function get_cart_items() {
        $total_items = 0;
        if (isset($_SESSION['cart']['items'])) {
            $total_items = count($_SESSION['cart']['items']);
        }
        return $total_items;
    }

    private function go_securepay($order_no, $total_amount) {

        $uid = '2690a3ef-acbc-449e-aae0-7ce58aedcc07';
        $checksum_token = '635c0154e352c2627ce34655a86a6a0d9c51131fe4b9a67ffb2fa49f1a0ba694';
        $auth_token = 'LM1xUk1-QJb6XNiCzFwH';
        $url = 'https://sandbox.securepay.my/api/v1/payments';

        $order_number = $order_no;
        $buyer_name = $this->request->getPost('nama_penuh');
        $buyer_phone = $this->request->getPost('nombor_telefon');
        $buyer_email = $this->request->getPost('email');
        $product_description = 'Bayaran untuk Order '. $order_no;
        $transaction_amount = $total_amount;
        $callback_url = '';
        $redirect_url = site_url('checkout/redirect/securepay');
        $redirect_post = "true";

        $string = $buyer_email."|".$buyer_name."|".$buyer_phone."|".$callback_url."|".$order_number."|".$product_description."|".$redirect_url ."|".$transaction_amount."|".$uid;

        $sign = hash_hmac('sha256', $string, $checksum_token);


        $post_data = "buyer_name=".urlencode($buyer_name)."&token=". urlencode($auth_token) 
        ."&callback_url=".urlencode($callback_url)."&redirect_url=". urlencode($redirect_url) . 
        "&order_number=".urlencode($order_number)."&buyer_email=".urlencode($buyer_email).
        "&buyer_phone=".urlencode($buyer_phone)."&transaction_amount=".urlencode($transaction_amount).
        "&product_description=".urlencode($product_description)."&redirect_post=".urlencode($redirect_post).
        "&checksum=".urlencode($sign);	

        $payments_model = new \App\Models\PaymentsModel();

        $payments_model->insert([
            'payment_gateway' => 'securepay',
            'status' => 'pending',
            'data' => $post_data,
            'order_no' => $order_no
        ]);

        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url);        
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        curl_exec($ch);
        
        $output = curl_exec($ch);
        
        return $output;
    }

    public function redirect( $payment_gateway ) {
        if ($payment_gateway == 'securepay') {
            if ($this->request->getPost('payment_status') == "true") {
                // process success here
                $payments_model = new \App\Models\PaymentsModel();
                $orders_model = new \App\Models\OrdersModel();
    
                $order_no = $this->request->getPost('order_number');

                $payment = $payments_model->where('order_no', $order_no)->first();
                $order = $orders_model->where('order_no', $order_no)->first();

                $payment_data = $payment['data'];
                $payment_data .= "\n\n". json_encode($_POST);
                
                $payment['data'] = $payment_data;
                $payment['status'] = 'success';

                $payments_model->update($payment['id'], $payment);

                $order = $orders_model->where('order_no', $order_no)->first();

                $order['status'] = 'success';
                $orders_model->update($order['id'], $order );

                return redirect()->to('/checkout/success/'. $order_no);

            } else {
                return redirect()->to('/checkout/error');
            }

            
        } else {
            echo "Page not found";
        }
    }



    public function success($order_no) {

        $orders_model = new \App\Models\OrdersModel();
        $orderitems_model = new \App\Models\OrderItemsModel();
        $product_model = new \App\Models\ProductModel();

        $order = $orders_model->where('order_no', $order_no)->first();

        if (!$order || ($order['status'] != 'success')) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        } 
        else {
                $this->data['order'] = $order;
                $this->data['order_items'] = $orderitems_model->where('order_id', $order['id'])->findAll();

                $product_ids = [];
                foreach($this->data['order_items'] as $items) {
                $product_ids[] = $items['product_id'];
            }

            $this->data['product'] = $product_model->find( $product_ids );

            return view('checkout/success', $this->data);
    
        }

    }

    public function error() {
        return view('checkout/error');
    }






    public function process_checkout() {

        if (!isset($_SESSION['cart']['items']) || (count($_SESSION['cart']['items']) < 1) ) {
            return redirect()->back();
        }



        $this->validation->setRule('nama_penuh', 'Nama Penuh', 'required');
        $this->validation->setRule('nombor_telefon', 'Nombor Telefon', 'required');
        $this->validation->setRule('email', 'Email', 'required|valid_email');
        $this->validation->setRule('alamat_1', 'Alamat', 'required');
        $this->validation->setRule('poskod', 'Poskod', 'required|numeric');
        $this->validation->setRule('daerah', 'Daerah', 'required');
        $this->validation->setRule('negeri', 'Negeri', 'required');

        if ( $this->validation->run($_POST) ) {
            helper('text');
            $orders_model = new \App\Models\OrdersModel();
            $orderitems_model = new \App\Models\OrderItemsModel();
            $order_no = random_string('alnum', 10);
            $total_amount = $this->get_cart_total_amount();

            $order_id = $orders_model->insert([
                'order_no' => $order_no,
                'nama_penuh' => $this->request->getPost('nama_penuh'),
                'email' => $this->request->getPost('email'),
                'nombor_telefon' => $this->request->getPost('nombor_telefon'),
                'alamat_1'=> $this->request->getPost('alamat_1'),
                'alamat_2' => $this->request->getPost('alamat_2'),
                'daerah' => $this->request->getPost('daerah'),
                'poskod'=> $this->request->getPost('poskod'), 
                'negeri' => $this->request->getPost('negeri'),
                'total_amount' => $total_amount,
                'items' => $this->get_cart_items(),
                'qty' => $this->get_cart_qty(),
                'status' => 'pending payment'      
            ]);

            foreach($_SESSION['cart']['items'] as $k => $items ) {
                $orderitems_model->insert([
                    'order_id' => $order_id,
                    'Product_id' => $items['id'],
                    'Price' => $items['Price'],
                    'qty' => $items['qty']
                ]);
            }

            $redirect_url = $this->go_securepay( $order_no, $total_amount);
            echo $redirect_url;
            //dd($redirect_url);

            // masukkan data dalam orders
            // masukkan juga data dalam payments

            // dd($order_id);



        } else {

            $_SESSION['form_error'] = $this->validation->getErrors();
            //dd($err);

            $_SESSION['POST'] = $_POST;

            $_SESSION['error'] = true;
            $this->session->markAsFlashdata('error');    
            $this->session->markAsFlashdata('form_error');    
            $this->session->markAsFlashdata('POST');    
            return redirect()->back();
            
        }
    }



    
}

