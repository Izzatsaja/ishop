<?php

/**
 * --------------------------------------------------------------------
 * CODEIGNITER 4 - SimpleAuth
 * --------------------------------------------------------------------
 *
 * This content is released under the MIT License (MIT)
 *
 * @package    SimpleAuth
 * @author     GeekLabs - Lee Skelding 
 * @license    https://opensource.org/licenses/MIT	MIT License
 * @link       https://github.com/GeekLabsUK/SimpleAuth
 * @since      Version 1.0
 * 
 */

 namespace App\Controllers;

class Superadmin extends BaseController
{
	public function index()
	{
		$data = [];
		$db = \Config\Database::connect();

		
		// SUM(orders.qty) `total_qty`, 
		// SUM(orders.items) `total_items`,
		
		$res = $db->query('SELECT 
		DATE_FORMAT(created_at, "%Y-%m-%d") AS `day_date`, 
		SUM(orders.total_amount) `main_total`, 
		COUNT(*) `total_orders`
	FROM orders
	WHERE 
	orders.created_at >= "2021-03-01 00:00:00"
	AND orders.created_at <= "2021-03-31 23:59:59"
	GROUP BY DATE_FORMAT(created_at, "%Y-%m-%d")');

		///dd($res->getResultArray());

		return view('admin/chart', [ 'res' => $res->getResultArray() ]);

	}

	//--------------------------------------------------------------------

}
