<?php

namespace App\Controllers;

class Product extends BaseController
{

    var $product_img_location = 'img/product/';

	function __construct() {
		$this->session = session();
		$this->product_model = new \App\Models\ProductModel();
		$this->category_model = new \App\Models\CategoryModel();
	}

	public function homepage() {

        $category = $this->category_model->dropdown();
		$data = [
			'product' => $this->product_model->orderBy('id', 'desc')->paginate(10),
			'pager' => $this->product_model->pager,
			'product_img_location' => $this->product_img_location,
			'category' => $category
		];

		return view('product_homepage', $data);

    }


	public function index()
	{
		$category = $this->category_model->dropdown();

        $data = [
            'product' => $this->product_model->orderBy('id', 'asc')->paginate(5),
            'pager' => $this->product_model->pager,
			'category' => $category
        ];

		return view('admin_product/listing', $data );
	}

	function edit($id) {

	 	$product = $this->product_model->find( $id );
		$category = $this->category_model->dropdown();

		return view('admin_product/edit', [ 
			'category' => $category,
            'product' => $product,
            'product_img_location' => $this->product_img_location
        ]);
	}

     function slug($slug) {
         $product = $this->product_model->where('slug', $slug)->first();
	 	return view('admin_product/edit', [ 
             'product' => $product,
             'product_img_location' => $this->product_img_location
         ]);
     }

	function save_edit($id) {
		$data = [
			'Nama' => $this->request->getPost('Nama'),
			'Description' => $this->request->getPost('Description'),
			'Price' => $this->request->getPost('Price')
		];

		if ($this->request->getPost('category_id') != '0') {
			$data['category_id'] = $this->request->getPost('category_id');
		}


		$file = $this->request->getFile('Picture');
        //dd($file);
		// Grab the file by name given in HTML form
		if ($file-> isReadable())
		{		
			// Generate a new secure name
			$file_picture = $file->getRandomName();
		
			// Move the file to it's new home
			$file->move( $this->product_img_location , $file_picture);

			$data['Picture'] = $file_picture;
		
		}

        //dd($data);
		$this->product_model->update($id, $data);

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/product/edit/'. $id);

	}


	function delete( $id ) {
		$this->product_model->where('id', $id)->delete();

		$_SESSION['deleted'] = true;
		$this->session->markAsFlashdata('deleted');

		return redirect()->back();
	}

	function add() {		
		$category = $this->category_model->dropdown();

		return view('admin_product/add', [ 'category' => $category ]);

	}

	// untuk save data dari add new form
	function save_new() {
		$data = [
			'Nama' => $this->request->getPost('Nama'),
			'Description' => $this->request->getPost('Description'),
			'Price' => $this->request->getPost('Price')
		];

		if ($this->request->getPost('category_id') != '0') {
			$data['category_id'] = $this->request->getPost('category_id');
		}


		$file = $this->request->getFile('Picture');

		//dd($files);

		// Grab the file by name given in HTML form
		if ($file-> isReadable())
		{		
			// Generate a new secure name
			$file_picture = $file->getRandomName();
		
			// Move the file to it's new home
			$file->move($this->product_img_location, $file_picture);

			$data['Picture'] = $file_picture;
		
		}

		$this->product_model->insert( $data );

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/product');

		// echo "<h1>HELOO ... saya akan save data</h1>";
	}
}
