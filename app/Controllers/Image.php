<?php

namespace App\Controllers;

class Image extends BaseController
{


	 function __construct() {
	 	$this->session = \Config\Services::session();
	 }

	public function index()
	{
		

		$image_model = new \App\Models\ImageModel();

		//$image = $image_model -> paginate();
          $data = [
              'image' => $image_model->orderBy('id', 'asc')->paginate(4),
              'pager' => $image_model->pager,
         ];
		
		
		
		 return view('admin/listing',$data);
	}

	 function edit($id) {
		helper('form');
	 	$image_model = new \App\Models\ImageModel();
	  	//$image = $image_model->find( $id );
		$rekod = $image_model->find( $id );

	 	//return view('admin/edit', [ 'Image' => $image ]);
		 return view('admin/edit', [ 'Image' => $rekod ]);
	 }

	 function save_edit($id) {
	 	$image_model = new \App\Models\ImageModel();

	 	$data = [
	 		'Nama' => $this->request->getPost('Nama'),
	 		'Description' => $this->request->getPost('Description')
	 	];

	 	$file = $this->request->getFile('Image_loc');

	 	// Grab the file by name given in HTML form
	 	if ($file)
	 	{		
	 		// Generate a new secure name
	 		$Image_loc = $file->getRandomName();
		
	 		// Move the file to it's new home
	 		$file->move('img/', $Image_loc);

	 		$data['Image_loc'] = $Image_loc;
		
	 	}
	 	$image_model->update($id, $data);

	 	 $_SESSION['success'] = true;
	 	 $this->session->markAsFlashdata('success');

	 	 return redirect()->to('/image/edit/'. $id);

	 }


	 function delete( $id ) {
	 	$image_model = new \App\Models\ImageModel();

	 	$image_model->where('id', $id)->delete();

	 	$_SESSION['deleted'] = true;
	 	$this->session->markAsFlashdata('deleted');

	 	return redirect()->back();
	 }

	 function add() {	
		 helper('form');	
	 	return view('admin/add');
	}

	// // untuk save data dari add new form
	function save_new() {
         $image_model = new \App\Models\ImageModel();
		
	 	$data = [
		'Nama' => $this->request->getPost('Nama'),
		'Description' => $this->request->getPost('Description')
		];

 		$file = $this->request->getFile('Image_loc');

		//dd($files);
	

	 	// Grab the file by name given in HTML form
	 	if ($file)
	 	{		
	 		// Generate a new secure name
	 		$Image_loc = $file->getRandomName();
		
			// Move the file to it's new home
	 		$file->move('img/', $Image_loc);

	 		$data['Image_loc'] = $Image_loc;
		
	 	}

	 	$image_model->insert( $data );
		$session = \Config\Services::session();

	 	$_SESSION['success'] = true;
	 	$this->session->markAsFlashdata('success');

	 	return redirect()->to('/Image');

	
	 }
}
