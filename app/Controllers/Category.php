<?php

namespace App\Controllers;

class Category extends BaseController
{

	function __construct() {
		$this->session = session();
		$this->category_model = new \App\Models\CategoryModel();
	}

    

	public function index()
	{

        $data = [
            'category' => $this->category_model->orderBy('id', 'desc')->paginate(10),
            'pager' => $this->category_model->pager,
        ];

		return view('admin_category/listing', $data );
	}

	function edit($id) {

	 	$category = $this->category_model->find( $id );

		return view('admin_category/edit', [ 
            'category' => $category
        ]);
	}

    function slug($slug) {
        $category = $this->category_model->where('slug', $slug)->first();
		return view('admin_category/edit', [ 
            'category' => $category
        ]);
    }

	function save_edit($id) {
		$data = [
			'Nama' => $this->request->getPost('Nama'),
			'Description' => $this->request->getPost('Description'),
			'Price' => $this->request->getPost('Price')
		];

		$this->category_model->update($id, $data);

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/category/edit/'. $id);

	}


	function delete( $id ) {
		$this->category_model->where('id', $id)->delete();

		$_SESSION['deleted'] = true;
		$this->session->markAsFlashdata('deleted');

		return redirect()->back();
	}

	function add() {		
		return view('admin_category/add');
	}

	// untuk save data dari add new form
	function save_new() {
		$data = [
			'Nama' => $this->request->getPost('Nama'),
			'Description' => $this->request->getPost('Description'),
			'Price' => $this->request->getPost('Price')
		];

		$this->category_model->insert( $data );

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/category');

		// echo "<h1>HELOO ... saya akan save data</h1>";
	}
}
