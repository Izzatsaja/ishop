<?php

namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model
{
    protected $table      = 'category';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['Nama'];

    // protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

     function dropdown() {
         $category = $this->findAll();
         $to_return = [ '' => '[ -- Please choose category -- ]' ];
         foreach($category as $cat) {
             $to_return[ $cat['id'] ] = $cat['Nama'];
         }

         return $to_return;
     }

}